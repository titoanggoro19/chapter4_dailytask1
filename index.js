// 1. buat array of object students ada 5 data cukup
const students = [
  {
    name: "Tito Anggoro",
    province: "Jawa Timur",
    age: 22,
    status: "single",
  },
  {
    name: "Tom Holland",
    province: "Jawa Barat",
    age: 19,
    status: "single",
  },
  {
    name: "John Wick",
    province: "Kalimantan Tengah",
    age: 45,
    status: "married",
  },
  {
    name: "Tony Stark",
    province: "Jambi",
    age: 77,
    status: "married",
  },
  {
    name: "Bruce Wayne",
    province: "Bengkulu",
    age: 50,
    status: "single",
  },
];
// console.log(student);

// 2. 3 function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak

function ProvinceisJawaBarat(province) {
  let isprovince = "";
  if (province == "Jawa Barat") {
    isprovince = "Jawa Barat";
  } else {
    isprovince = "Luar Jawa Barat";
  }
  return isprovince;
}

// console.log(ProvinceisJawaBarat("Jawa Barat"));

function ageover(age) {
  let isage = "";
  if (age < 22) {
    isage = "Dibawah 22";
  } else {
    isage = "Diatas 22";
  }
  return isage;
}

// console.log(ageover(22))

function statusis(status) {
  let isstatus = "";
  if (status == "single") {
    isstatus = "single";
  } else {
    isstatus = "married";
  }
  return isstatus;
}

// console.log(statusis("single"))

// 3. callback function untuk print hasil proses 3 function diatas.

function printresult(callback1, callback2, callback3) {
  for (const element of students) {
    // console.log(element);
    let printprovince = callback1(element.province);
    // console.log(element.provinsi);
    let printage = callback2(element.age);
    let printstatus = callback3(element.status);
    if (
      printprovince == "Jawa Barat" &&
      printage == "Dibawah 22" &&
      printstatus == "single"
    ) {
      console.log(
        `Nama Saya adalah ${element.name}, Saya tinggal di ${printprovince}, Umur Saya ${printage} tahun, dan status saya ${printstatus}.`
      );
    }
  }
}

printresult(ProvinceisJawaBarat, ageover, statusis);
